##Welcome to the wide, wide world of Ruby on Rails

This is a Ruby on Rails book that is built for the sole purpose of teaching people who have very little/no programming skill at all

Required reading/classes/experience/etc:

- [tryruby.org](http://tryruby.org).
- [ruby.learncodethehardway.org](http://ruby.learncodethehardway.org/).

If you go through those two (in that order), you will have the skills to go forth and follow this.

After going through 20 pages of google search results, and finding nothing useful that was licensed for commercial use, I decided to write one. Enjoy the fruits of my labors, and please, file a Github issue if you have problems.

[![CC-BY](http://i.creativecommons.org/l/by/3.0/88x31.png)](http://creativecommons.org/licenses/by/3.0/deed.en_US)

This license means you have the rights to do the following:
  - Share — to copy, distribute and transmit the work.
  - Remix — to adapt the work.
  - make commercial use of the work.
So long as you:
  -  Provide attribution — You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work). 

For the purposes of this discussion, attribution is a required pink banner at the top of every page - er, no, that'd just be mean.

Attribution is the following snippet:

> Originally written by James Gifford <james@jamesrgifford.com>.

Toss this into either your README, your Preface, or the credits. Whatever. I just need to be able to be mentioned somewhere. 

I'd also appreciate it if you emailed me a copy of whatever you build on top of this. I think it'd be really fun to read what people build on top of this. Anyway.

This is built with [The Web Book Boilerplate](https://github.com/PascalPrecht/wbb). They do an awesome job, you should go star them and take a look at how awesome they are.

Any questions? No? Good, because adventure awaits you!